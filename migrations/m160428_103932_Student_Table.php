<?php

use yii\db\Migration;

class m160428_103932_Student_Table extends Migration
{
    public function up()
    {
        $this->createTable(
            'Students',
            [
                'id' => 'pk',
                'name' => 'string',
                'demo_date' => 'date',
                'notes' => 'text',
            ],
            'ENGINE=InnoDB'
        );
    }
    public function down()
    {
        $this->dropTable('demo');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
