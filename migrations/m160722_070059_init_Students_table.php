<?php

use yii\db\Migration;

class m160722_070059_init_Students_table extends Migration
{
public function up()
    {
        $this->createTable(
            'Students',
            [
                'id' => 'pk',
                'name' => 'string',
                'demo_date' => 'date',
                'notes' => 'text',
            ],
            'ENGINE=InnoDB'
        );
    }
    public function down()
    {
        $this->dropTable('demo');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
