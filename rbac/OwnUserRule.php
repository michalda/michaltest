<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnUserRule extends Rule
{
	public $name = 'ownUserRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			//die('logged in user ='.$user.'worked on user = '.$params['user']->id);
			//die (isset($params['user']) ? $params['user']->id == $user : false); 
			return isset($params['user']) ? $params['user']->id == $user : false;
		}
		return false;
	}
}
