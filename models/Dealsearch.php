<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Deal;

/**
 * Dealsearch represents the model behind the search form about `app\models\Deal`.
 */
class Dealsearch extends Deal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadid', 'amount'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
     public function search($params)
    {
        
		
		$this->load($params);
		
		
		if (!isset($this->amount) || $this->amount >= -1){
			$query = Deal::find()
			
			;
		}else{
			$query = Deal::find()
			->select("*")
			->join('LEFT OUTER JOIN','auth_assignment','auth_assignment.user_id = deal.id')
			->where(['auth_assignment.item_name' =>$this->amount ]);			
		}
			
		

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
		

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		 $this->amount == -1 ? $this->amount = null : $this->amount; 
		 // $this->leadid == -1 ? $this->leadid = null : $this->leadid; 

        // grid filtering conditions
		
        $query->andFilterWhere([
            'id' => $this->id,
            'leadid' => $this->leadid,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
	
        return $dataProvider;
    }
}
