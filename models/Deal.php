<?php

namespace app\models;
use yii\helpers\ArrayHelper;	
use Yii;


/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadid
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadid', 'name', 'amount'], 'required'],
            [['leadid', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadid' => 'Leadid',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	public static function getAmount()
	{
		$amount = ArrayHelper::
					map(self::find()->all(), 'id', 'amount');
		return $amount;						
	}
//	public static function getDealWithAllAmounts()
	//{
		//$amount = self::getAmount();
	//	$amount[-1] = 'All amount';
		//$amount = array_reverse ( $amount, true );
		//return $amount;	
	//}
	public function getFullname()
    {
        return $this->amount;
    }
	public static function getStatuses()
	{
		$alllead = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($alllead, 'id', 'name');
		return $allStatusesArray;						
	}
	public static function getLeadWithAllName()
	{
		$alllead = self::getStatuses();
		$alllead[-1] = 'All Statuses';
		$alllead = array_reverse ( $alllead, true );
		return $alllead;	
	}
	
	
	

}
